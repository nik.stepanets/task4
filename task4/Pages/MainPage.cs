﻿using OpenQA.Selenium;

namespace task4.Pages
{
    internal class MainPage : BasePage
    {
        IWebElement EbayLogo => driver.FindElement(By.XPath("//img[@id='gh-logo']"));
        IWebElement ShopByCategoriesButton => driver.FindElement(By.Id("gh-shop-a"));
        IWebElement SeeAllCategoriesLink => driver.FindElement(By.Id("gh-shop-see-all"));

        public MainPage(IWebDriver driver) : base(driver)
        {
        }

        public bool IsLogoDisplayed()
        {
            return EbayLogo.Displayed;
        }

        public void OpenAllGategoriesPage()
        {
            ShopByCategoriesButton.Click();
            SeeAllCategoriesLink.Click();
        }
    }
}
