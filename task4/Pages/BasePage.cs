﻿using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;
using System;

namespace task4.Pages
{
    abstract internal class BasePage
    {
        protected IWebDriver driver;
        protected BasePage(IWebDriver driver)
        {
            this.driver = driver;
        }

    }
}
