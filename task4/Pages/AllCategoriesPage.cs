﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Text;

namespace task4.Pages
{
    internal class AllCategoriesPage : BasePage
    {
        IWebElement AllCategoriesNavMenu => driver
            .FindElement(By.XPath("//div[contains(@class, 'all-categories-left-nav-container')]"));
        public AllCategoriesPage(IWebDriver driver) : base(driver)
        {
        }

        public bool IsCategoriesListDisplayed()
        {
            return AllCategoriesNavMenu.Displayed;
        }
    }
}
