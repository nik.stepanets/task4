﻿using NUnit.Framework;
using task4.Base;
using task4.Pages;

namespace task4.Tests
{
    [TestFixture(BrowserType.Firefox)]
    [TestFixture(BrowserType.Chrome)]
    [Parallelizable(scope: ParallelScope.Fixtures)]
    internal class EbayTests : BaseTest
    {
        public EbayTests(BrowserType browser)
        {
            browserType = browser;
        }

        [Test]
        public void TestLogoDisplayed()
        {
            MainPage mainPage = new MainPage(Driver);
            Assert.That(mainPage.IsLogoDisplayed(), Is.True);
        }

        [Test]
        public void OpenAllCategoriesPage()
        {
            MainPage mainPage = new MainPage(Driver);
            mainPage.OpenAllGategoriesPage();
            AllCategoriesPage allCategoriesPage = new AllCategoriesPage(Driver);
            Assert.That(allCategoriesPage.IsCategoriesListDisplayed(), Is.True);
        }
    }
}
