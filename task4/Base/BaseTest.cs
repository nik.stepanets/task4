﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Safari;
using RestSharp;
using System;
using System.Net;
using System.Threading;

namespace task4.Base
{
    internal class BaseTest
    {
        public static string HUB_URL = "http://localhost:4444/wd/hub";
        static readonly bool remoteWebDriver = CheckIfServerIsRunning(HUB_URL);

        protected BrowserType browserType = BrowserType.Chrome;
        private static readonly ThreadLocal<IWebDriver> DriverThreadLocal = new ThreadLocal<IWebDriver>(true);

        public static IWebDriver Driver
        {
            get
            {
                if (!DriverThreadLocal.IsValueCreated)
                    throw new NullReferenceException("WebDriver not initialized!");
                return DriverThreadLocal.Value;
            }
            private set
            { 
                DriverThreadLocal.Value = value;
            }
        }

        static bool CheckIfServerIsRunning(string url)
        {
            RestClient client = new RestClient(url);
            RestRequest request = new RestRequest("/status", Method.Get);
            RestResponse response = client.Execute(request);

            return response != null && response.StatusCode.Equals(HttpStatusCode.OK);
        }

        [SetUp]
        public void Setup()
        {
            if (remoteWebDriver)
            {
                Uri uri = new Uri(HUB_URL);
                DriverOptions options = browserType switch
                {
                    BrowserType.Firefox => new FirefoxOptions(),
                    BrowserType.Chrome => new ChromeOptions(),
                    BrowserType.Safari => new SafariOptions(),
                    BrowserType.Edge => new EdgeOptions(),
                    _ => new ChromeOptions(),
                };
                Driver = new RemoteWebDriver(uri, options);
            }
            else
            {
                Driver = browserType switch
                {
                    BrowserType.Firefox => new FirefoxDriver(),
                    BrowserType.Chrome => new ChromeDriver(),
                    BrowserType.Safari => new SafariDriver(),
                    BrowserType.Edge => new EdgeDriver(),
                    _ => new ChromeDriver(),
                };
            }

            Driver.Manage().Timeouts().ImplicitWait.Add(TimeSpan.FromSeconds(30));
            Driver.Manage().Window.Maximize();
            Driver.Navigate().GoToUrl("https://www.ebay.com/");
        }

        [TearDown]
        public void CleanUp()
        {
            Driver?.Quit();
        }

        public enum BrowserType
        {
            Chrome,
            Firefox,
            Edge,
            Safari
        }
    }
}
